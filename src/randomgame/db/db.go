package db

import (
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type DataStore struct {
	Session *mgo.Session
}
type UserData struct {
	ID        bson.ObjectId `bson:"_id,omitempty"`
	Login     string
	Email     string
	Password  string
	SessionId string
	WinStat   map[string]string
	WinRate   float32
}

func (report *UserData) Add(s mgo.Session) error {
	session := s.Copy()
	defer session.Close()

	c := session.DB("randomgame").C("users")
	err := c.Insert(report)
	if err != nil {
		return err
	}
	return nil
}
func (d DataStore) GetUserData(field, value string) (UserData, error) {
	session := d.Session.Copy()
	defer session.Close()

	c := session.DB("randomgame").C("users")
	ud := UserData{}
	err := c.Find(bson.M{field: value}).One(&ud)
	if err != nil {
		return ud, err
	}
	return ud, nil
}
func (d DataStore) GetToplist() ([]UserData, error) {
	session := d.Session.Copy()
	defer session.Close()
	c := session.DB("randomgame").C("users")
	ud := []UserData{}
	err := c.Find(nil).Sort("-winrate").Limit(15).All(&ud)
	if err != nil {
		return ud, err
	}
	return ud, nil
}
func (d DataStore) Update(username string, newData UserData) error {
	session := d.Session.Copy()
	defer session.Close()

	c := session.DB("randomgame").C("users")
	err := c.Update(bson.M{"login": username}, &newData)
	if err != nil {
		return err
	}
	return nil
}
func (d *DataStore) Init(ip string) error {
	session, err := mgo.Dial(ip)
	if err != nil {
		return err
	}
	session.SetMode(mgo.Monotonic, true)
	d.Session = session
	return nil
}
func (d *DataStore) Close() {
	d.Session.Close()
}
