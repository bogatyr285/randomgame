package game

import (
	"log"
	"math/rand"
	"randomgame/db"
	"strconv"
	"time"
)

const TOTAL_TRY = 7

type UserSession struct {
	MagicNumber   int //what we wished
	TryCount      int
	StartGameTime int64 //to reset try count if user away
}
type UserAnswer struct {
	WantGame bool   `json:"wantgame,omitempty"`
	TryGuess string `json:"tryguess,omitempty"`
}
type ServerAnswer struct {
	State     string `json:"state,omitempty"` //will be: less-enter number less than we wishes, more-more, win - win, obosralsya - lose, newgame- new game rdy
	RemainTry string `json:"remaintry,omitempty"`
}

var UserList map[string]*UserSession = make(map[string]*UserSession)

func newGame() *UserSession {
	rand.Seed(time.Now().UnixNano())
	return &UserSession{
		TryCount:      0,
		MagicNumber:   rand.Intn(100),
		StartGameTime: time.Now().Unix(),
	}
}
func ReqParser(userAnswer UserAnswer, key string, ds db.DataStore) (serverAnswer ServerAnswer, err error) {
	userData, err := ds.GetUserData("sessionid", key)
	if err != nil {
		log.Printf("[ReqParser] DB err %v", err)
		return
	}

	if userAnswer.WantGame {
		UserList[key] = newGame()
		serverAnswer.State = "newgame"
		return
	}
	if userAnswer.TryGuess != "" {
		if UserList[key].StartGameTime+120 < time.Now().Unix() { // if user so long be away
			UserList[key] = newGame()
		}
		userNum, err := strconv.Atoi(userAnswer.TryGuess)
		if err != nil {
			log.Printf("Err parse user num %v", err)
			return ServerAnswer{}, err
		}
		if userNum > UserList[key].MagicNumber {
			UserList[key].TryCount++
			serverAnswer.State = "more"
			remaintTry := strconv.Itoa(TOTAL_TRY - UserList[key].TryCount)
			serverAnswer.RemainTry = remaintTry
		} else if userNum < UserList[key].MagicNumber {
			UserList[key].TryCount++
			serverAnswer.State = "less"
			remaintTry := strconv.Itoa(TOTAL_TRY - UserList[key].TryCount)
			serverAnswer.RemainTry = remaintTry
		}
		if UserList[key].TryCount == TOTAL_TRY {
			serverAnswer.State = "obosralsya"
			remaintTry := strconv.Itoa(TOTAL_TRY - UserList[key].TryCount)
			serverAnswer.RemainTry = remaintTry

			//pornography time. don't do to do that, use mongo increment. i haven't time to this
			cntLoseInt, _ := strconv.Atoi(userData.WinStat["0"])
			cntLoseInt++
			cntLoseStr := strconv.Itoa(cntLoseInt)
			userData.WinStat["0"] = cntLoseStr
		} else if UserList[key].MagicNumber == userNum {
			serverAnswer.State = "win"
			tryCount := strconv.Itoa(UserList[key].TryCount + 1)
			serverAnswer.RemainTry = tryCount

			cntWinInt, _ := strconv.Atoi(userData.WinStat[tryCount])
			cntWinInt++
			cntWinStr := strconv.Itoa(cntWinInt)
			userData.WinStat[tryCount] = cntWinStr
		}
		rating := calcRating(userData)
		userData.WinRate = rating

		err = ds.Update(userData.Login, userData)
		log.Printf("Userdata before insert%+v", userData)
		if err != nil {
			log.Printf("[ReqParser] insert db err %v", err)
			return ServerAnswer{}, err
		}

		log.Printf("%+v", serverAnswer)
		for _, v := range UserList {
			log.Printf("%+v", v)
		}
	}
	return
}
func calcRating(data db.UserData) (res float32) {

	for key, val := range data.WinStat {

		try, _ := strconv.Atoi(key)
		cntWin, _ := strconv.Atoi(val)
		log.Printf("%+v\n", try, cntWin)

		if try != 0 {
			res += float32((TOTAL_TRY - try + 1) * cntWin)
		} else {
			res--
		}

	}
	return
}
