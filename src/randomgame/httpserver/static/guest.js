const TOTAL_TRY=7
var g={
    magicNumber:0,
    countTry:0,
    NewGame:function(){
        this.magicNumber=Math.round(Math.random() * 99 + 1),
        this.countTry=0
    }
}


$(document).ready(()=> {
    $("#answer").val('')
  
    $('#sendResult').click(()=> {
        var answer = $("#answer").val()
        $("#answer").val('')
        if (answer.length==0){
                return
        }
        answerParser(answer)    
    });
      $('#newGame').click(()=> {
         $('#newGame').hide()
         $("#phase1").animate({ opacity: 1 }, 500, "linear")
         var text="Enter number and pray"
         $('#messages').text(text);
         g.NewGame()
     });
});

function answerParser(num){ 
    remainTry=TOTAL_TRY-g.countTry-1
    if (num>g.magicNumber){
             g.countTry++
               var text="Entered number is more. Number of attempts: "+remainTry
            $('#messages').text(text); 
            
    }else if(num<g.magicNumber){
            g.countTry++
            var text="Entered number is less. Number of attempts: "+remainTry
            $('#messages').text(text);  
    }
    if(g.countTry==TOTAL_TRY){
           var text="You lose(((99. Maybe will be lucky next time"
             $('#messages').text(text);    
             $("#phase1").animate({ opacity: 0 }, 500, "linear")
             $('#newGame').show();

    }else if(num==g.magicNumber){          
            var text="You won with "+(g.countTry+1)+" attempts! Congratulations!"
            $('#messages').text(text);  
            $("#phase1").animate({ opacity: 0 }, 500, "linear")          
            $('#newGame').show()             
    }   
}