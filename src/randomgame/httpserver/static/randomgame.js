
$(document).ready(function () {
    $("#answer").val('')
    console.log("READY")
    $('#sendResult').click(() => {
        var answer = $("#answer").val()
        $("#answer").val('')
        if (answer.length == 0) {
            return
        }
        var dataToSend = {
            "tryguess": answer
        }
        sendAjax(dataToSend)
    });
    $('#newGame').click(() => {
        $('#newGame').hide()
        var dataToSend = {
            "wantgame": true
        }
        sendAjax(dataToSend)
    });
    /*register */
    $('#login-form-link').click(function (e) {
        $("#login-form").delay(100).fadeIn(100);
        $("#register-form").fadeOut(100);
        $('#register-form-link').removeClass('active');
        $(this).addClass('active');
        e.preventDefault();
    });
    $('#register-form-link').click(function (e) {
        $("#register-form").delay(100).fadeIn(100);
        $("#login-form").fadeOut(100);
        $('#login-form-link').removeClass('active');
        $(this).addClass('active');
        e.preventDefault();
    });
   
    // http://www.runningcoder.org/jqueryvalidation/demo/
    $('#login-form').validate({
        submit: {
            settings: {
                clear: 'keypress',
                inputContainer: '.form-group',
                errorListClass: 'error-list',
                scrollToError: true,
            },
            callback: {
                onBeforeSubmit: function (node) {

                    console.log("beforesub node:")
                    console.log(node)

                },
                onSubmit: function (node, formData) {           
                    $.ajax({
                        type: 'POST',
                        data: {
                            username: formData.username,
                            password: formData.password
                        },
                        url: '/login',
                        dataType: 'json',
                        success: function (data, textStatus, jqxhr) {
                            var response = JSON.parse(data)
                            console.log(response.err)
                            if (response.err) {
                                $('<div class="error-list" data-error-list="" ></div>').insertAfter("#password")
                                
                                $('.error-list').html('<ul><li>' + response.err + '</li></ul>')
                            }
                            if (response.ok){
                                history.go()
                            } 
                        }
                    });             
                   
                }
            }
        }

    });


    // dynamic: {
    //     settings: {
    //         trigger: 'keyup',
    //         delay: 1000
    //     },
    //     callback: {
    //         onSuccess: function(node, input, keyCode) {
    //             var inputName = $(input).attr('name');
    //             if ($.inArray(inputName, ['username', 'password']) === -1) {
    //                 console.log("RETURN")
    //                 return false;
    //             }
    //             var password = $('#password').val()
    //             $.ajax({
    //                 type: 'POST',
    //                 data: {
    //                     username: inputName,
    //                     password: password
    //                 },
    //                 url: '/login',
    //                 dataType: 'json',
    //                 success: function(data, textStatus, jqxhr) {
    //                     var response = JSON.parse(data)
    //                     console.log(response.err)
    //                     if (response.err) {

    //                         $(input).css("background-color", "red")
    //                         // $('#login-form').addError({
    //                         //      'error-list': response.err,
    //                         // })
    //                         $('.error-list').html('<ul><li>' + response.err + '</li></ul>')

    //                     } else {
    //                         //history.go()
    //                         $(input).css("background-color","green")
    //                         // $(input).parent().find('.error-list').addClass('green');

    //                     }
    //                 }
    //             });

    //         },
    //         onError: function(node, input, keyCode, error) {
    //             console.log(input)
    //             $(input).css("background-color", "red")
    //         }
    //     }
    // }

    $('#register-form').validate({
        submit: {
            settings: {
                clear: 'keypress',
                inputContainer: '.form-group',
                errorListClass: 'error-list',
                scrollToError: true,
            }
        }
    });
    $('#quit').click(() => {
        document.cookie = "sessionId="//delete cookie
        location.reload()
    })

});

function sendAjax(dataToSend) {
    $.ajax({
        url: 'receive',
        type: 'post',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(dataToSend),
        success: (data) => {
            answerParser(data)
        },
        failure: (errMsg) => {
            alert(errMsg);
        }
    });
}

function answerParser(data) {
    var d = JSON.parse(JSON.stringify(data))
    switch (d.state) {
        case "newgame":
            $("#phase1").animate({
                opacity: 1
            }, 500, "linear")
            var text = "Enter number and pray"
            $('#messages').text(text);
            break;
        case "win":
            var text = "You won with " + d.remaintry + " attempts! Congratulations!"
            $('#messages').text(text);
            $("#phase1").animate({
                opacity: 0
            }, 500, "linear")
            $('#newGame').show()
            break;
        case "obosralsya":
            var text = "You lose(((99. Maybe will be lucky next time"
            $('#messages').text(text);
            $("#phase1").animate({
                opacity: 0
            }, 500, "linear")
            $('#newGame').show();
            break;
        case "more":
            var text = "Entered number greater than wished. Number of attempts: " + d.remaintry
            $('#messages').text(text);
            break;
        case "less":
            var text = "Entered number less than wished. Number of attempts: " + d.remaintry
            $('#messages').text(text);
            break;
    }
}