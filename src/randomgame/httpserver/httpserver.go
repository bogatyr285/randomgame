package httpserver

import (
	"encoding/json"
	"log"
	"net/http"
	"randomgame/db"
	"randomgame/game"
	"randomgame/utils"
	"text/template"
	"time"
)

var ds db.DataStore //save DB session

const (
	COOKIE_NAME = "sessionId"
	MONGO_IP    = "127.0.0.1"
)

func Serve(address, port string) {
	log.Printf("Http server port:%v", port)

	http.Handle("/httpserver/static/", http.StripPrefix("/httpserver/static/", http.FileServer(http.Dir("./httpserver/static/"))))

	http.HandleFunc("/", indexPage)
	http.HandleFunc("/login", loginHandler)
	http.HandleFunc("/guest", guestHandler)
	http.HandleFunc("/register", registerHandler)
	http.HandleFunc("/receive", receiveAjax)
	http.HandleFunc("/top15", top15Handler)

	err := ds.Init(MONGO_IP)
	if err != nil {
		log.Printf("Init DB error %v", err)
	}
	defer ds.Close()

	log.Fatal(http.ListenAndServe(address+":"+port, nil))
}
func top15Handler(w http.ResponseWriter, r *http.Request) {
	topList, err := ds.GetToplist()
	if err != nil {
		log.Printf("Get toplist err %v ", err)
		json.NewEncoder(w).Encode(`{"err":"Internal err "}`)
		return
	}
	log.Printf("%+v", topList)
	t, err := template.ParseFiles("httpserver/templates/toplist.html", "httpserver/templates/header.html", "httpserver/templates/footer.html")
	if err != nil {
		log.Printf("Parse template err %v ", err)
		json.NewEncoder(w).Encode(`{"err":"Internal err "}`)
		return
	}

	t.ExecuteTemplate(w, "toplist", topList)
}
func indexPage(w http.ResponseWriter, r *http.Request) {
	cookie, err := r.Cookie(COOKIE_NAME)
	log.Printf("Recieve cookie:%v", cookie)
	if err == nil {
		_, err := ds.GetUserData("sessionid", cookie.Value)
		if err != nil {
			if err.Error() == "not found" { //кука устарела мб, кидаем на авторизацию и снимаем куку

				cookie := &http.Cookie{
					Name:   COOKIE_NAME,
					MaxAge: -1, //delete cookie
				}
				http.SetCookie(w, cookie)
				http.Redirect(w, r, "/", 302)
				return
			}
			log.Printf("Get from DB err %v", err)
			json.NewEncoder(w).Encode(`{"err":"Get from DB. ` + err.Error() + `"}`)
			//TODO there need beautiful error/404 page, but not today
			return
		}

		t, err := template.ParseFiles("httpserver/templates/index.html", "httpserver/templates/header.html", "httpserver/templates/footer.html", "httpserver/templates/game.html", "httpserver/templates/register.html")
		if err != nil {
			log.Print(err)
		}
		t.ExecuteTemplate(w, "index", struct{ GameReady string }{GameReady: "true"})
		return
	}

	t, err := template.ParseFiles("httpserver/templates/index.html", "httpserver/templates/header.html", "httpserver/templates/footer.html", "httpserver/templates/register.html", "httpserver/templates/game.html")
	if err != nil {
		log.Print(err)
	}
	t.ExecuteTemplate(w, "index", struct{ GameReady string }{GameReady: "false"})
}
func guestHandler(w http.ResponseWriter, r *http.Request) {
	t, err := template.ParseFiles("httpserver/templates/guest.html")
	if err != nil {
		log.Print(err)
	}
	t.ExecuteTemplate(w, "guest", nil)
}
func loginHandler(w http.ResponseWriter, r *http.Request) {
	log.Print("loginHandler")
	username := r.FormValue("username")
	password := r.FormValue("password")
	log.Printf("%v %v", username, password)
	if username == "" || password == "" {
		json.NewEncoder(w).Encode(`{"err":"Server doen't recieve login data."}`)
		return
	}

	userData, err := ds.GetUserData("login", username)
	if err != nil {
		log.Printf("Get from DB err %v", err)
		json.NewEncoder(w).Encode(`{"err":"Incorrect login. ` + err.Error() + `"}`)
		return
	}

	pswd := utils.GetMD5Hash(password)
	if userData.Password != pswd {
		json.NewEncoder(w).Encode(`{"err":"Incorrect password."}`)
		return
	}

	sessionId := utils.GenerateId()
	userData.SessionId = sessionId
	err = ds.Update(username, userData)
	if err != nil {
		log.Printf("Update SessionId in DB err %v", err)
		json.NewEncoder(w).Encode(`{"err":"Update DB ` + err.Error() + `"}`)
		return
	}

	cookie := &http.Cookie{
		Name:    COOKIE_NAME,
		Value:   sessionId,
		Expires: time.Now().Add(5 * time.Minute),
	}
	http.SetCookie(w, cookie)
	json.NewEncoder(w).Encode(`{"ok":"welcome"}`)
}
func registerHandler(w http.ResponseWriter, r *http.Request) {
	log.Print("registerHandler")
	username := r.FormValue("username")
	email := r.FormValue("email")
	password := r.FormValue("password")
	log.Printf("%v %v %v", username, password, email)

	if username == "" || email == "" || password == "" {
		json.NewEncoder(w).Encode(`{"err":"Server doen't recieve registration data."}`)
		return
	}

	existData, err := ds.GetUserData("login", username)
	if err != nil {
		log.Print(err.Error())
		log.Print(len(err.Error()))
		if err.Error() != "not found" {
			log.Printf("Get from DB err %v", err)
			json.NewEncoder(w).Encode(`{"err":"Get from DB. ` + err.Error() + `"}`)
			return
		}
	}
	if existData.Login != "" {
		json.NewEncoder(w).Encode(`{"err":"Username already exist. "}`)
		return
	}

	pswd := utils.GetMD5Hash(password)
	sessionId := utils.GenerateId()
	userData := db.UserData{
		Login:     username,
		Email:     email,
		Password:  pswd,
		SessionId: sessionId,
	}
	err = userData.Add(*ds.Session)

	if err != nil {
		log.Printf("insert err %v", err)
		json.NewEncoder(w).Encode(`{"err":"Insert DB. Registration failed.` + err.Error() + `"}`)
		return
	}
	cookie := &http.Cookie{
		Name:    COOKIE_NAME,
		Value:   sessionId,
		Expires: time.Now().Add(5 * time.Minute),
	}
	http.SetCookie(w, cookie)
	http.Redirect(w, r, "/", 302)

}
func receiveAjax(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		cookie, err := r.Cookie(COOKIE_NAME)
		if err != nil {
			log.Print("[receiveAjax.Error]User send ajax without cookie!?")
			json.NewEncoder(w).Encode(`{"err":"Internal server error"}`)
			return
		}
		decoder := json.NewDecoder(r.Body)
		log.Print(r.Body)
		var userAnswer game.UserAnswer
		err = decoder.Decode(&userAnswer)

		if err != nil {
			log.Printf("Err decode user answer %v", err)
			return //TODO there need send err to user, but not today
		}
		key := cookie.Value
		serverAnswer, err := game.ReqParser(userAnswer, key, ds)
		if err != nil {
			log.Printf("ReqParse err %v", err)
		}

		json.NewEncoder(w).Encode(serverAnswer)
	}
}
